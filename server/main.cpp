/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// reference: https://github.com/opencv/opencv/blob/master/samples/cpp/stereo_match.cpp

#include <opencv2/core/mat.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/stereo.hpp>
#include <opencv2/ximgproc/disparity_filter.hpp>
#include <opencv2/core/ocl.hpp>

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <sys/stat.h>	// for mkdir
#include <sys/types.h>	// for mode_t in mkdir

#include <modal_pipe_server.h>
#include <modal_pipe_client.h>
#include <modal_start_stop.h>

// ignore config file for now, everything hard-coded
// #include "config_file.h"

using namespace cv;
using namespace cv::ocl;
using namespace cv::ximgproc;
using namespace stereo;
using namespace std;

#define PROCESS_NAME		"voxl-dfs-server"	// for process id

// pipe client configuration
// 8MiB pipe big enough for 13 VGA stereo frames
#define MIN_CAM_PIPE_SIZE (8*1024*1024)
#define CLIENT_NAME		"voxl-dfs-server"	// for pipes
static char cam_name[] = "/run/mpa/stereo/";

// pipe server channels to publish to
#define DISPARITY_PIPE_CH			0
#define DISPARITY_PIPE_NAME			(MODAL_PIPE_DEFAULT_BASE_DIR "dfs_disparity/")
#define POINTS_PIPE_CH				1
#define POINTS_PIPE_NAME			(MODAL_PIPE_DEFAULT_BASE_DIR "dfs_point_cloud/")
#define TARGET_POINT_PIPE_CH		2
#define TARGET_POINT_PIPE_NAME		(MODAL_PIPE_DEFAULT_BASE_DIR "dfs_target_point/")
#define TARGET_OVERLAY_PIPE_CH		3
#define TARGET_OVERLAY_PIPE_NAME	(MODAL_PIPE_DEFAULT_BASE_DIR "dfs_target_overlay/")

#define IMAGE_SAVE_DIR	"/data/dfs/"

Mat M1, D1, M2, D2;				// intrinsic matrices and distortions
Mat R, T, R1, P1, R2, P2;		// extrinsic rotation and translation
Mat map11, map12, map21, map22;	// undistortion maps
Rect roi1, roi2;
Mat Q;

// command line arg flags
int en_debug = 0;
int config_only = 0;
int en_save_files = 0;

// config
int n_to_skip = 3;
int disparity_levels = 32; // use 32 for full vga
int block_size = 5;
int uniqueness_ratio = 15;
int en_speckle_filter = 1;
int blur_size = 9;
const int scale = 2; // resolution divider, 1 for VGA, 2 for qVGA
const int w_scaled = 640/scale;
const int h_scaled = 480/scale;

// algorithms
Ptr<StereoBM> bm;
Rect wls_roi;
Ptr<DisparityWLSFilter> wls_filter;
Ptr<Feature2D> blob_detector;



static void _print_usage(void)
{
	printf("\n\
voxl-dfs-server usually runs as a systemd background service. However, for debug\n\
purposes it can be started from the command line manually with any of the following\n\
debug options. When started from the command line, voxl-dfs-server will automatically\n\
stop the background service so you don't have to stop it manually\n\
\n\
-c, --config             load the config file only, for use by the config wizard\n\
-d, --debug              run in debug mode which computes everything even when there\n\
                           are no clients to receive the data. Prints how long each\n\
                           computation step takes for performance monitoring\n\
-s, --save-files         save copies of disparity and target_overlay to /data/\n\
                           Only use for debugging, not in normal operation.\n\
-h, --help                print this help message\n\
\n");
	return;
}


static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"config",                no_argument,       0, 'c'},
		{"debug",                 no_argument,       0, 'd'},
		{"help",                  no_argument,       0, 'h'},
		{"save-files",            no_argument,       0, 's'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "cdhs", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'c':
			config_only = 1;
			break;

		case 'd':
			printf("enabling debug mode\n");
			en_debug = 1;
			break;

		case 'h':
			_print_usage();
			return -1;

		case 's':
			printf("enabling save files mode\n");
			en_save_files = 1;
			break;

		default:
			_print_usage();
			return -1;
		}
	}

	return 0;
}

static int64_t _apps_time_monotonic_ns()
{
	struct timespec ts;
	if(clock_gettime(CLOCK_MONOTONIC, &ts)){
		fprintf(stderr,"ERROR calling clock_gettime\n");
		return -1;
	}
	return (int64_t)ts.tv_sec*1000000000 + (int64_t)ts.tv_nsec;
}


// call this instead of return when it's time to exit to cleans up everything
static void _quit(int ret)
{
	pipe_server_close_all();
	pipe_client_close_all();
	remove_pid_file(PROCESS_NAME);
	if(ret==0) printf("Exiting Cleanly\n");
	exit(ret);
	return;
}


// just for debugging opencv MAT types
// this is a useful tool that could be put somewhere else
static void _print_type(int type){
	string r;

	uchar depth = type & CV_MAT_DEPTH_MASK;
	uchar chans = 1 + (type >> CV_CN_SHIFT);

	switch ( depth ) {
		case CV_8U:  r = "8U"; break;
		case CV_8S:  r = "8S"; break;
		case CV_16U: r = "16U"; break;
		case CV_16S: r = "16S"; break;
		case CV_32S: r = "32S"; break;
		case CV_32F: r = "32F"; break;
		case CV_64F: r = "64F"; break;
		default:     r = "User"; break;
	}

	r += "C";
	r += (chans+'0');

	cout << r << endl;
}


static Rect compute_wls_roi(int w, int h)
{
	int min_disparity = 0;
	int bs2 = block_size/2;
	int minD = min_disparity, maxD = min_disparity + disparity_levels - 1;

	int xmin = maxD + bs2;
	int xmax = w + minD - bs2;
	int ymin = bs2;
	int ymax = h - bs2;

	Rect r(xmin, ymin, xmax - xmin, ymax - ymin);
	return r;
}


// camera frame callback registered to voxl-camera-server
static void _frame_handler(__attribute__((unused))int ch, camera_image_metadata_t meta, char* frame, __attribute__((unused)) void* context)
{
	static int skip_ctr = 0;

	if(meta.format != IMAGE_FORMAT_STEREO_RAW8){
		fprintf(stderr, "ERROR only support STEREO_RAW_8 images right now\n");
		return;
	}

	// check if we have a mandatory skip
	if(skip_ctr<n_to_skip){
		skip_ctr++;
		if(en_debug){
			fprintf(stderr, "Skipping frame due to required skip\n");
		}
		return;
	}

	// check if we got backed up and skip a frame if so
	if(pipe_client_bytes_in_pipe(0)>0){
		skip_ctr ++;
		if(en_debug){
			fprintf(stderr, "Skipping frame due to backup\n");
		}
		return;
	}

	// going forward with processing, reset skip counter
	skip_ctr = 0;

	// if we have no clients and not in debug mode, no need to do anything
	if(!en_debug											&& \
		pipe_server_get_num_clients(DISPARITY_PIPE_CH)		<=0 && \
		pipe_server_get_num_clients(POINTS_PIPE_CH)			<=0 && \
		pipe_server_get_num_clients(TARGET_POINT_PIPE_CH)	<=0 && \
		pipe_server_get_num_clients(TARGET_OVERLAY_PIPE_CH)	<=0)
	{
		return;
	}

	// make opencv Mat images from raw frame data
	int w_raw = meta.width;
	int h_raw = meta.height;
	Mat img1_raw(h_raw, w_raw, CV_8UC1, frame);
	Mat img2_raw(h_raw, w_raw, CV_8UC1, frame + (w_raw*h_raw));

	// make sure there wasn't a problem making the mat images
	if(img1_raw.empty() || img2_raw.empty()){
		fprintf(stderr, "failed to construct opencv Mat from frame\n");
		return;
	}

	if(en_save_files){
		printf("saving " IMAGE_SAVE_DIR "img1.png\n");
		imwrite(IMAGE_SAVE_DIR "img1_raw.png", img1_raw);
		printf("saving " IMAGE_SAVE_DIR "img2.png\n");
		imwrite(IMAGE_SAVE_DIR "img2_raw.png", img2_raw);
	}

	// in debug mode keep track of the processing time for each step
	int64_t t_start, t_now, t_last;
	if(en_debug){
		t_start = _apps_time_monotonic_ns();
		t_last = t_start;
		printf("--------------------------------------------\n");
		printf("starting processing for new frame w:%d h:%d\n", w_raw, h_raw);
	}


////////////////////////////////////////////////////////////////////////////////
// No more resizing, do block match on full VGA res always
////////////////////////////////////////////////////////////////////////////////

	Mat img1_resized, img2_resized;
	if(scale != 1){
		if(en_debug) t_last = _apps_time_monotonic_ns();
		int method = INTER_LINEAR; // other options: INTER_AREA INTER_CUBIC
		resize(img1_raw, img1_resized, Size(), 1.0/scale, 1.0/scale, method);
		resize(img2_raw, img2_resized, Size(), 1.0/scale, 1.0/scale, method);
		if(en_debug){
			t_now = _apps_time_monotonic_ns();
			printf("%6.2fms   resize time\n", ((double)(t_now-t_last))/1000000.0);
			t_last = t_now;
		}
	}
	else{
		img1_resized = img1_raw;
		img2_resized = img1_raw;
	}
	int w_resized = img1_resized.cols;
	int h_resized = img1_resized.rows;
	if(en_debug){
		printf("new resized image for disparity is w:%d h:%d\n", w_resized, h_resized);
	}

	if(en_save_files){
		printf("saving " IMAGE_SAVE_DIR "img1_resized.png\n");
		imwrite(IMAGE_SAVE_DIR "img1_resized.png", img1_resized);
		printf("saving " IMAGE_SAVE_DIR "img2_resized.png\n");
		imwrite(IMAGE_SAVE_DIR "img2_resized.png", img2_resized);
	}

////////////////////////////////////////////////////////////////////////////////
// undistortion and rectification step, maps were generated already.
// Do this before blur since we need a sharp rectified image for the WLS filter
////////////////////////////////////////////////////////////////////////////////

	if(en_debug) t_last = _apps_time_monotonic_ns();

	Mat img1_rect, img2_rect;
	remap(img1_resized, img1_rect, map11, map12, INTER_LINEAR);
	remap(img2_resized, img2_rect, map21, map22, INTER_LINEAR);

	if(en_debug){
		t_now = _apps_time_monotonic_ns();
		printf("%6.2fms   undistort time\n", ((double)(t_now-t_last))/1000000.0);
		t_last = t_now;
	}

	if(en_save_files){
		printf("saving " IMAGE_SAVE_DIR "img1_rect.png\n");
		imwrite(IMAGE_SAVE_DIR "img1_rect.png", img1_rect);
		printf("saving " IMAGE_SAVE_DIR "img2_rect.png\n");
		imwrite(IMAGE_SAVE_DIR "img2_rect.png", img2_rect);
	}


////////////////////////////////////////////////////////////////////////////////
// gaussian blur
////////////////////////////////////////////////////////////////////////////////

	if(en_debug) t_last = _apps_time_monotonic_ns();

	Mat img1_blur, img2_blur;
	//GaussianBlur(img1_rect, img1_blur, Size(blur_size,blur_size), 0, 0);
	//GaussianBlur(img2_rect, img2_blur, Size(blur_size,blur_size), 0, 0);
	blur(img1_rect, img1_blur, Size(blur_size,blur_size));
	blur(img2_rect, img2_blur, Size(blur_size,blur_size));

	// time how long the resize took in debug mode
	if(en_debug){
		t_now = _apps_time_monotonic_ns();
		printf("%6.2fms   blur time\n", ((double)(t_now-t_last))/1000000.0);
		t_last = t_now;
	}

	if(en_save_files){
		printf("saving " IMAGE_SAVE_DIR "img1_blur.png\n");
		imwrite(IMAGE_SAVE_DIR "img1_blur.png", img1_blur);
		printf("saving " IMAGE_SAVE_DIR "img2_blur.png\n");
		imwrite(IMAGE_SAVE_DIR "img2_blur.png", img2_blur);
	}


////////////////////////////////////////////////////////////////////////////////
// compute disparity with block matcher
////////////////////////////////////////////////////////////////////////////////

	// do the disparity block match
	if(en_debug) t_last = _apps_time_monotonic_ns();

	// // Umat/opencl version
	// UMat disp_raw;
	// printf("converting to umat\n");
	// UMat img1_u, img2_u;
	// // img1_u = img1_blur.getUMat(ACCESS_RW, USAGE_ALLOCATE_SHARED_MEMORY);
	// // img2_u = img2_blur.getUMat(ACCESS_RW, USAGE_ALLOCATE_SHARED_MEMORY);
	// img1_blur.copyTo(img1_u);
	// img2_blur.copyTo(img2_u);
	// if(en_debug) printf("computing disparity with opencl\n");
	// bm->compute(img1_u, img2_u, disp_raw);

	// CPU version
	if(en_debug) printf("computing disparity with CPU\n");
	Mat disp_raw;
	bm->compute(img1_blur, img2_blur, disp_raw);


	if(en_debug){
		t_now = _apps_time_monotonic_ns();
		printf("%6.2fms   disparity time\n", ((double)(t_now-t_last))/1000000.0);
		t_last = t_now;
	}

	if(en_save_files){
		printf("saving " IMAGE_SAVE_DIR "disp_raw.png\n");
		imwrite(IMAGE_SAVE_DIR "disp_raw.png", disp_raw);
	}

////////////////////////////////////////////////////////////////////////////////
// do a min/max trim which gets rid of noise in some cases
////////////////////////////////////////////////////////////////////////////////

	// int16_t* d = (int16_t*)disp_raw.data;
	// int n_pixels = disp_raw.rows * disp_raw.cols;
	// int16_t limit = disparity_levels*(16-1);
	// for(int i=0; i<n_pixels; i++){
	// 	if(d[i]>limit) d[i]=limit;
	// 	else if(d[i]<0) d[i]=0;
	// }

	// if(en_debug){
	// 	t_now = _apps_time_monotonic_ns();
	// 	printf("%6.2fms   min/max trim time\n", ((double)(t_now-t_last))/1000000.0);
	// 	t_last = t_now;
	// }

////////////////////////////////////////////////////////////////////////////////
// WLS Filter
////////////////////////////////////////////////////////////////////////////////

	Mat disp_filtered;
	if(en_debug) t_last = _apps_time_monotonic_ns();
	wls_filter->filter(disp_raw, img1_rect, disp_filtered, Mat(), wls_roi);
	if(en_debug){
		t_now = _apps_time_monotonic_ns();
		printf("%6.2fms   WLS filter time\n", ((double)(t_now-t_last))/1000000.0);
		t_last = t_now;
	}
	if(en_save_files){
		printf("saving " IMAGE_SAVE_DIR "disp_filtered.png\n");
		imwrite(IMAGE_SAVE_DIR "disp_filtered.png", disp_filtered);
	}

////////////////////////////////////////////////////////////////////////////////
// make an 8-bit version of disparity for use with blob detector
////////////////////////////////////////////////////////////////////////////////

	Mat disp8; // 8-bit disparity

	if(en_debug													|| \
		pipe_server_get_num_clients(DISPARITY_PIPE_CH)		>0	|| \
		pipe_server_get_num_clients(TARGET_POINT_PIPE_CH)	>0	|| \
		pipe_server_get_num_clients(TARGET_OVERLAY_PIPE_CH)	>0)

	{
		if(en_debug) t_last = _apps_time_monotonic_ns();

		// convert to 8-bit
		disp_filtered.convertTo(disp8, CV_8UC1, 255.0/(disparity_levels *16.0));

		if(en_debug){
			t_now = _apps_time_monotonic_ns();
			printf("%6.2fms   8-bit conversion time\n", ((double)(t_now-t_last))/1000000.0);
			t_last = _apps_time_monotonic_ns();
		}

		if(en_save_files){
			printf("saving " IMAGE_SAVE_DIR "disp8.png\n");
			imwrite(IMAGE_SAVE_DIR "disp8.png", disp8);
		}
	}


////////////////////////////////////////////////////////////////////////////////
// publish 8-bit disparity to pipe
////////////////////////////////////////////////////////////////////////////////

	if(pipe_server_get_num_clients(DISPARITY_PIPE_CH)>0){
		// construct metadata
		camera_image_metadata_t disp_meta = meta;
		disp_meta.format = IMAGE_FORMAT_RAW8;
		disp_meta.height = disp_filtered.rows;
		disp_meta.width = disp_filtered.cols;
		disp_meta.size_bytes = disp_meta.height * disp_meta.width;
		disp_meta.stride = disp_meta.width;
		pipe_server_send_camera_frame_to_channel(DISPARITY_PIPE_CH, disp_meta, (char*)disp8.data);

		if(en_debug){
			t_now = _apps_time_monotonic_ns();
			printf("%6.2fms   8-bit disparity pipe write time\n", ((double)(t_now-t_last))/1000000.0);
			t_last = t_now;
		}
	}



////////////////////////////////////////////////////////////////////////////////
// if in debug mode or we have a client for the point cloud data, calculate it
////////////////////////////////////////////////////////////////////////////////

	// if(en_debug || pipe_server_get_num_clients(POINTS_PIPE_CH) > 0){

	// 	// make floating point version of disparity map and point cloud
	// 	t_last = _apps_time_monotonic_ns();
	// 	Mat xyz;
	// 	Mat disp_float;
	// 	disp_filtered.convertTo(disp_float, CV_32F, 1.0f / 16.0f);
	// 	reprojectImageTo3D(disp_float, xyz, Q, true);
	// 	if(en_debug){
	// 		t_now = _apps_time_monotonic_ns();
	// 		printf("%6.2fms   generate 3d point cloud time\n", ((double)(t_now-t_last))/1000000.0);
	// 		t_last = t_now;
	// 	}

	// 	if(en_debug){
	// 		printf("generated %d x %d 3d points\n", xyz.rows, xyz.cols);
	// 	}

	// 	// make a point cloud metadata struct and send out with the points
	// 	point_cloud_metadata_t pc_meta;
	// 	pc_meta.magic_number = POINT_CLOUD_MAGIC_NUMBER;
	// 	pc_meta.timestamp_ns = meta.timestamp_ns;
	// 	pc_meta.n_points = xyz.rows * xyz.cols;
	// 	pc_meta.reserved = 0;

	// 	// send to pipe
	// 	pipe_server_send_point_cloud_to_channel(POINTS_PIPE_CH, pc_meta, xyz.data);
	// }



////////////////////////////////////////////////////////////////////////////////
// target detection
////////////////////////////////////////////////////////////////////////////////

	vector<KeyPoint>  keypoints;

	if(en_debug || pipe_server_get_num_clients(TARGET_POINT_PIPE_CH)	> 0 \
				|| pipe_server_get_num_clients(TARGET_OVERLAY_PIPE_CH)	> 0)
	{
		if(en_debug) t_last = _apps_time_monotonic_ns();

		// run the blob detector
		blob_detector->detect(disp8, keypoints, Mat());

		if(en_debug){
			t_now = _apps_time_monotonic_ns();
			printf("%6.2fms   blob detection time\n", ((double)(t_now-t_last))/1000000.0);
			t_last = t_now;
		}

		////////////////////////////////////////////////////////////////////////
		// find closest object (max disparity) by looking for the max disparity
		// in a few pixels around the center of each blob
		////////////////////////////////////////////////////////////////////////
		#define RADIUS 7
		int16_t max_disp = 0;
		float center_x, center_y, blob_size;
		float target_point[3];
		int has_found = 0;
		float relative_size_final = 0.0;
		float distance_final = 0.0;

		for(vector<KeyPoint>::iterator k = keypoints.begin(); k != keypoints.end(); ++k){

			// make sure point isn't too close to the edge so we can safely check
			// the points around the center. This is very unlikely.
			int cx = k->pt.x;
			int cy = k->pt.y;

			if(en_debug) printf("\nchecking blob at %d %d\n", cx, cy);

			if(cx<(RADIUS) || cx>=(w_scaled-RADIUS) || cy<(RADIUS) || cy>=(h_scaled-RADIUS)){
				if(en_debug) printf("skipping point too close to edge\n");
				continue;
			}

			// search small area around center
			int16_t local_max = 0;
			int i,j;
			for(i=-RADIUS; i<=RADIUS; i++){
				for(j=-RADIUS; j<=RADIUS; j++){
					int16_t tmp = disp_filtered.at<int16_t>(cy+i, cx+j);
					if(tmp>local_max) local_max = tmp;
				}
			}

			// convert 16-bit disparity to float
			float max_disp_f = (float)local_max / 16.0f;

			// calculate 3d point location
			cv::Mat_<double> vec_tmp(4,1);
			vec_tmp(0) = (float)cx;
			vec_tmp(1) = (float)cy;
			vec_tmp(2) = max_disp_f;
			vec_tmp(3) = 1;
			float point[3];
			vec_tmp = Q*vec_tmp;
			vec_tmp /= vec_tmp(3);
			point[0] = vec_tmp(0);
			point[1] = vec_tmp(1);
			point[2] = vec_tmp(2);

			// check that distance is reasonable
			float distance = sqrtf(point[0]*point[0] + point[1]*point[1] + point[2]*point[2]);
			if(distance<1.0f){
				if(en_debug) printf("skipping point, distance too close <1m\n");
				continue;
			}
			if(distance>20.0f){
				if(en_debug) printf("skipping point, distance too far >20m\n");
				continue;
			}

			// check that relative size is reasonable
			float relative_size = k->size * (float)scale * distance;
			if(relative_size>220.0f){
				if(en_debug) printf("skipping point, size too big\n");
				continue;
			}
			if(relative_size<70.0f){
				if(en_debug) printf("skipping point, size too small\n");
				continue;
			}

			// check if the max in this little area is the overall max
			if(local_max>max_disp){
				max_disp = local_max;
				center_x = (float)cx;
				center_y = (float)cy;
				blob_size = k->size;
				has_found = 1;
				target_point[0] = point[0];
				target_point[1] = point[1];
				target_point[2] = point[2];
				distance_final = distance;
				relative_size_final = relative_size;
			}
		}

		if(en_debug){
			if(has_found){
				printf("found object, relative_size: %f distance: %f\n", (double)relative_size_final, (double)distance_final);
			}
			else{
				printf("didn't find object\n");
			}
			t_now = _apps_time_monotonic_ns();
			printf("%6.2fms   object search time\n", ((double)(t_now-t_last))/1000000.0);
			t_last = t_now;
		}


		////////////////////////////////////////////////////////////////////////
		// if we found something and have a target_point subscriber, publish single point
		////////////////////////////////////////////////////////////////////////
		if(has_found && pipe_server_get_num_clients(TARGET_POINT_PIPE_CH)>0){

			if(en_debug){
				printf("sending target point to pipe: %f %f %f\n", (double)target_point[0], (double)target_point[1], (double)target_point[2]);
			}

			// make a point cloud metadata struct and send out with the points
			point_cloud_metadata_t target_pc_meta;
			target_pc_meta.magic_number = POINT_CLOUD_MAGIC_NUMBER;
			target_pc_meta.timestamp_ns = meta.timestamp_ns;
			target_pc_meta.n_points = 1;
			target_pc_meta.reserved = 0;

			// send to pipe
			pipe_server_send_point_cloud_to_channel(TARGET_POINT_PIPE_CH, target_pc_meta, target_point);
		}



		////////////////////////////////////////////////////////////////////////
		// draw box around target on original image if requested
		////////////////////////////////////////////////////////////////////////
		if(en_debug || pipe_server_get_num_clients(TARGET_OVERLAY_PIPE_CH)>0){

			// new image to draw keypoints on
			if(en_debug) t_last = _apps_time_monotonic_ns();
			//Mat target_overlay(img1_rect);
			Mat target_overlay(img1_raw);

			// only draw target if we found one
			if(has_found){
				// draw rectangle
				float size = blob_size*1.0f;
				Point top_left = Point((double)(center_x-size)*scale, (double)(center_y-size)*scale);
				Point btm_right = Point((double)(center_x+size)*scale, (double)(center_y+size)*scale);
				rectangle(target_overlay, top_left, btm_right, Scalar(255), 2);
				// draw depth in meters
				// char text[100];
				// sprintf(text, "xyz %0.1f %0.1f %0.1f", (double)target_point[0], (double)target_point[1], (double)target_point[2]);
				// putText(target_overlay, text, btm_right, FONT_HERSHEY_SIMPLEX , 1.0, Scalar(255), 2);
			}

			//publish target overlay
			camera_image_metadata_t overlay_meta = meta;
			overlay_meta.format = IMAGE_FORMAT_RAW8;
			overlay_meta.height = target_overlay.rows;
			overlay_meta.width  = target_overlay.cols;
			overlay_meta.size_bytes = overlay_meta.width * overlay_meta.height;
			overlay_meta.stride = overlay_meta.width;
			pipe_server_send_camera_frame_to_channel(TARGET_OVERLAY_PIPE_CH, overlay_meta, (char*)target_overlay.data);

			if(en_debug){
				t_now = _apps_time_monotonic_ns();
				printf("%6.2fms   target overlay draw time\n", ((double)(t_now-t_last))/1000000.0);
				t_last = _apps_time_monotonic_ns();
			}

			if(en_save_files){
				printf("saving " IMAGE_SAVE_DIR "target_overlay.png\n");
				imwrite(IMAGE_SAVE_DIR "target_overlay.png", target_overlay);
			}

		} // end of target overlay stuff

	} // end of target detection stuff

	if(en_debug){
		int64_t t_end = _apps_time_monotonic_ns();
		printf("total time:     %6.2fms\n", ((double)(t_end-t_start))/1000000.0);
	}

	return;
}


// called whenever we connect to the camera server
static void _camera_connect_cb( __attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf("connected to camera server\n");
	return;
}

// called whenever we disconnect from the camera server
static void _camera_disconnect_cb( __attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf("disconnected from camera server\n");
	return;
}


int main(int argc, char* argv[])
{
	if(_parse_opts(argc, argv)) return -1;

	// // load our  config file
	// if(!config_only) printf("loading config file: %s\n", VOXL_DFS_SERVER_CONF_FILE);
	// if(config_file_read()) return -1;
	if(config_only) return 0;
	// if(!config_only) config_file_print();

////////////////////////////////////////////////////////////////////////////////
// check opencv status
////////////////////////////////////////////////////////////////////////////////

	//std::cout << cv::getBuildInformation() << std::endl;
	// printf("checking openCL\n");
	// if (!cv::ocl::haveOpenCL()) {
	// 	std::cout << "OpenCL is not available" << std::endl;
	// }
	// else{
	// 	cv::ocl::setUseOpenCL(true);
	// 	cv::ocl::Context context;
	// 	if (!context.create(cv::ocl::Device::TYPE_ALL)) {
	// 		std::cout << "Failed creating the context..." << std::endl;
	// 	}
	// 	std::cout << context.ndevices() << " GPU devices are detected." << std::endl;
	// 	for (int i = 0; i < (int)context.ndevices(); i++) {
	// 		cv::ocl::Device device = context.device(i);
	// 		std::cout << "name:              " << device.name() << std::endl;
	// 		std::cout << "available:         " << device.available() << std::endl;
	// 		std::cout << "imageSupport:      " << device.imageSupport() << std::endl;
	// 		std::cout << "OpenCL_C_Version:  " << device.OpenCL_C_Version() << std::endl;
	// 		std::cout << std::endl;
	// 	}
	// }


////////////////////////////////////////////////////////////////////////////////
// gracefully handle an existing instance of the process and associated PID file
////////////////////////////////////////////////////////////////////////////////

	// make sure another instance isn't running
	// if return value is -3 then a background process is running with
	// higher privaledges and we couldn't kill it, in which case we should
	// not continue or there may be hardware conflicts. If it returned -4
	// then there was an invalid argument that needs to be fixed.
	if(kill_existing_process(PROCESS_NAME, 2.0)<-2) return -1;

	// start signal handler so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		_quit(-1);
	}

	// make PID file to indicate your project is running
	// due to the check made on the call to rc_kill_existing_process() above
	// we can be fairly confident there is no PID file already and we can
	// make our own safely.
	make_pid_file(PROCESS_NAME);

////////////////////////////////////////////////////////////////////////////////
// load calibration
////////////////////////////////////////////////////////////////////////////////

	// read intrinsic parameters
	std::string intrinsic_filename = "/data/modalai/opencv_stereo_intrinsics.yml";
	FileStorage fs(intrinsic_filename, FileStorage::READ);
	if(!fs.isOpened()){
		printf("Failed to open calibration file %s\n", intrinsic_filename.c_str());
		return -1;
	}
	fs["M1"] >> M1;
	fs["D1"] >> D1;
	fs["M2"] >> M2;
	fs["D2"] >> D2;
	if(scale != 1){
		M1 /= scale;
		M2 /= scale;
	}
	Size img_size(w_scaled, h_scaled);

	// open extrinsics file and generate rectification maps
	std::string extrinsic_filename = "/data/modalai/opencv_stereo_extrinsics.yml";
	fs.open(extrinsic_filename, FileStorage::READ);
	if(!fs.isOpened())
	{
		printf("Failed to open file %s\n", extrinsic_filename.c_str());
		return -1;
	}
	fs["R"] >> R;
	fs["T"] >> T;

	if(en_debug) printf("generating rectification map for w:%d h:%d\n", w_scaled, h_scaled);

	stereoRectify( M1, D1, M2, D2, img_size, R, T, R1, R2, P1, P2, Q, CALIB_ZERO_DISPARITY, -1, img_size, &roi1, &roi2 );
	initUndistortRectifyMap(M1, D1, R1, P1, img_size, CV_16SC2, map11, map12);
	initUndistortRectifyMap(M2, D2, R2, P2, img_size, CV_16SC2, map21, map22);

	// start making processor objects with desired settings
	bm = StereoBM::create(disparity_levels, block_size);
	bm->setROI1(roi1);
	bm->setROI2(roi2);
	bm->setPreFilterCap(31);
	bm->setPreFilterSize(9);
	bm->setPreFilterType(1);
	bm->setMinDisparity(0);
	bm->setTextureThreshold(0); // texture threshold must be ==0 for opencl
	bm->setUniquenessRatio(uniqueness_ratio);
	if(en_speckle_filter){
		bm->setSpeckleWindowSize(500);
		bm->setSpeckleRange(32);
	}
	else{
		bm->setSpeckleWindowSize(0);
		bm->setSpeckleRange(0);
	}
	bm->setDisp12MaxDiff(-1);

	// make a wls filter, default params seems to work fine
	double lambda = 8000.0;
	double sigma = 1.5;
	double ddr = 0.33;
	wls_filter = createDisparityWLSFilterGeneric(false); // use confidence == false
	wls_filter->setLambda(lambda);
	wls_filter->setSigmaColor(sigma);
	wls_filter->setDepthDiscontinuityRadius(ceil(ddr*block_size));
	wls_roi = compute_wls_roi(w_scaled, h_scaled);

	// make a blob detector
	SimpleBlobDetector::Params params;
	params.thresholdStep = 10; // 5 is better, but twice as slow
	params.minThreshold = 5;
	params.maxThreshold = 255;
	params.minRepeatability = 2;
	params.minDistBetweenBlobs = 10;
	params.filterByColor = false;
	params.blobColor = 0;
	params.filterByArea = true;
	params.minArea = 50/(scale*scale);
	params.maxArea = 5000/(scale*scale);
	params.filterByCircularity = false;
	params.minCircularity = 0.1f;
	params.maxCircularity = (float)1e37;
	params.filterByInertia = false;
	params.minInertiaRatio = 0.1f;
	params.maxInertiaRatio = (float)1e37;
	params.filterByConvexity = false;
	params.minConvexity = 0.95f;
	params.maxConvexity = (float)1e37;

	// make blob detector and do the detection
	blob_detector = SimpleBlobDetector::create(params);


	// make directory for debug image saves
	if(mkdir(IMAGE_SAVE_DIR, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) && errno!=EEXIST){
		perror("ERROR calling mkdir");
		printf("tried to make %s\n", IMAGE_SAVE_DIR);
		return -1;
	}

////////////////////////////////////////////////////////////////////////////////
// start the server pipes
////////////////////////////////////////////////////////////////////////////////

	// init disparity output pipe
	if(pipe_server_init_channel(DISPARITY_PIPE_CH, DISPARITY_PIPE_NAME, 0)){
		_quit(-1);
	}
	pipe_server_set_default_pipe_size(DISPARITY_PIPE_CH, 8*1024*1024);

	// // init point cloud output pipe
	// if(pipe_server_init_channel(POINTS_PIPE_CH, POINTS_PIPE_NAME, 0)){
	// 	_quit(-1);
	// }
	// pipe_server_set_default_pipe_size(POINTS_PIPE_CH, 8*1024*1024);

	// init target point output pipe
	if(pipe_server_init_channel(TARGET_POINT_PIPE_CH, TARGET_POINT_PIPE_NAME, 0)){
		_quit(-1);
	}
	pipe_server_set_default_pipe_size(TARGET_POINT_PIPE_CH, 1*1024*1024);

	// init target image overlay output pipe
	if(pipe_server_init_channel(TARGET_OVERLAY_PIPE_CH, TARGET_OVERLAY_PIPE_NAME, 0)){
		_quit(-1);
	}
	pipe_server_set_default_pipe_size(TARGET_OVERLAY_PIPE_CH, 4*1024*1024);

	// indicate to the soon-to-be-started thread that we are initialized
	// and running, this is an extern variable in start_stop.c
	main_running=1;


////////////////////////////////////////////////////////////////////////////////
// now subscribe to camera server pipe
////////////////////////////////////////////////////////////////////////////////

	// construct path
	char cam_pipe_path[128];
	sprintf(cam_pipe_path, "%s%s", MODAL_PIPE_DEFAULT_BASE_DIR, cam_name);
	if(pipe_client_construct_full_path(cam_name, cam_pipe_path)){
		fprintf(stderr, "ERROR failed to construct a pipe path from cam_name: %s\n", cam_name);
	}
	printf("waiting for camera server at: %s\n", cam_pipe_path);

	// assign callbacks
	pipe_client_set_camera_helper_cb(0, _frame_handler, NULL);
	pipe_client_set_connect_cb(0, _camera_connect_cb, NULL);
	pipe_client_set_disconnect_cb(0, _camera_disconnect_cb, NULL);

	// open connection to camera server in auto-reconnect mode
	int ret = pipe_client_init_channel(0, cam_pipe_path, CLIENT_NAME, \
				EN_PIPE_CLIENT_CAMERA_HELPER|EN_PIPE_CLIENT_AUTO_RECONNECT, 0);
	if(ret<0){
		fprintf(stderr, "ERROR: critical failure subscribing to pipe %s\n", cam_pipe_path);
		_quit(-1);
	}
	

////////////////////////////////////////////////////////////////////////////////
// main loop just waits and keeps trying to open pipes if they are closed
////////////////////////////////////////////////////////////////////////////////

	while(main_running){
		usleep(5000000);
	}


////////////////////////////////////////////////////////////////////////////////
// close everything
////////////////////////////////////////////////////////////////////////////////
	_quit(0);
	return 0;
}

