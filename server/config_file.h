/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#include <modal_json.h>

#define VOXL_DFS_SERVER_CONF_FILE "/etc/modalai/voxl-dfs-server.conf"

#define CONFIG_FILE_HEADER "\
/**\n\
 * This file contains configuration that's specific to voxl-dfs-server.\n\
 * Most of these settings directly correspond to opencv stereo parameters.\n\
 *\n\
 * Algorithm is one of bm, sgbm, hh, csgbm3way, or hh4\n\
 */\n"


static char cam_name[32];
static int skip_n_frames;
static int disparity_levels;
static double scale;
static int block_size;


static const int n_algs = 5;
static const char* alg_strings[] = {"bm", "sgbm", "hh", "sgbm3way", "hh4"};
enum { STEREO_BM=0, STEREO_SGBM=1, STEREO_HH=2, STEREO_3WAY=3, STEREO_HH4=4 };
static int algorithm;


/**
 * load the config file and populate the above extern variables
 *
 * @return     0 on success, -1 on failure
 */
static void config_file_print(void)
{
	printf("=================================================================\n");
	printf("cam_name:                        %s\n",    cam_name);
	printf("skip_n_frames:                   %d\n",    skip_n_frames);
	printf("disparity_levels:                %d\n",    disparity_levels);
	printf("scale:                           %f\n",    scale);
	printf("block_size:                      %d\n",    block_size);
	printf("algorithm:                       %s\n",    alg_strings[algorithm]);
	printf("=================================================================\n");
	return;
}


/**
 * @brief      prints the current configuration values to the screen
 *
 *             this includes all of the extern variables listed above. If this
 *             is called before config_file_load then it will print the default
 *             values.
 */
static int config_file_read(void)
{
	int ret = json_make_empty_file_with_header_if_missing(VOXL_DFS_SERVER_CONF_FILE, CONFIG_FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Creating new config file: %s\n", VOXL_DFS_SERVER_CONF_FILE);

	cJSON* parent = json_read_file(VOXL_DFS_SERVER_CONF_FILE);
	if(parent==NULL) return -1;


	json_fetch_string_with_default(	parent, "cam_name",			cam_name,	32,	"stereo");
	json_fetch_int_with_default(	parent, "skip_n_frames",	&skip_n_frames,		0);
	json_fetch_int_with_default(	parent, "disparity_levels",	&disparity_levels,	32);
	json_fetch_double_with_default(	parent, "scale",			&scale,				1.0);
	json_fetch_int_with_default(	parent, "block_size",		&block_size,		9);
	json_fetch_enum_with_default(	parent, "algorithm,",		&algorithm,	alg_strings, n_algs, 0);


	if(disparity_levels < 1 || disparity_levels % 16 != 0 ){
		printf("The max disparity (disparity_levels) must be a positive integer divisible by 16\n");
		return -1;
	}
	if(scale < 0){
		printf("The scale factor (scale) must be a positive floating-point number\n");
		return -1;
	}
	if(block_size < 1 || block_size % 2 != 1){
		printf("The block size (block_size)  must be a positive odd number\n");
		return -1;
	}


	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse config file %s\n", VOXL_DFS_SERVER_CONF_FILE);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		printf("The config file was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(VOXL_DFS_SERVER_CONF_FILE, parent, CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);
	return 0;
}






#endif // end #define CONFIG_FILE_H