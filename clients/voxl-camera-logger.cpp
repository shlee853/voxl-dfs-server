/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <sstream>
#include <unistd.h>
#include <unistd.h>		// for access()
#include <sys/stat.h>	// for mkdir
#include <sys/types.h>
#include <limits.h>		// for PATH_MAX
#include <getopt.h>

#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/utility.hpp>

#include <modal_pipe_client.h>
#include <modal_start_stop.h>

using namespace std;
using namespace cv;

#define CLIENT_NAME		"voxl-camera-logger"	// client name for pipes
#define MIN_PIPE_SIZE	(16*1024*1024) // 16MiB for now

static int en_mono;
static int en_stereo;
static int n_frames = 0; // leave at 0 to indicate log forever
static char cam_path[64];
static char cam_name[64];
static char save_dir[64] = "/data/images/";
static int start_index; // file index in sequence
static int current_index;

// printed if some invalid argument was given
static void __print_usage(void)
{
	printf("\n\
Tool to save images published through Modal Pipe Architecture to disk.\n\
By default, this saves images to /data/images/ since the /data/ parition is the\n\
largest partition on VOXL. You can change this with the -d argument.\n\
\n\
-c, --camera {name}       name of the monocular camera to read, e.g. tracking0\n\
-d, --directory {dir}     name of the directory to save the images to, default\n\
                            is /data/images/ if argument is not provided.\n\
-h, --help                print this help message\n\
-n, --n_frames {frames}   number of frames to save before automatically exiting.\n\
                            otherwise it will run untill you quit the program.\n\
-s, --stereo {name}       name of the stereo camera to read, e.g. stereo0\n\
\n\
typical use:\n\
  yocto:/# voxl-camera-logger -c tracking0 -n 1\n\
\n");
	return;
}


static int _mkdir(const char *dir)
{
	char tmp[PATH_MAX];
	char* p = NULL;

	snprintf(tmp, sizeof(tmp),"%s",dir);
	for(p = tmp + 1; *p!=0; p++){
		if(*p == '/'){
			*p = 0;
			if(mkdir(tmp, S_IRWXU) && errno!=EEXIST){
				perror("ERROR calling mkdir");
				printf("tried to mkdir %s\n", tmp);
				return -1;
			}
			*p = '/';
		}
	}
	return 0;
}


static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"camera",                required_argument, 0, 'c'},
		{"directory",             required_argument, 0, 'd'},
		{"help",                  no_argument,       0, 'h'},
		{"n_frames",              required_argument, 0, 'n'},
		{"stereo",                required_argument, 0, 's'},
		{0, 0, 0, 0}
	};

	while(1){
		int len;
		int option_index = 0;
		int c = getopt_long(argc, argv, "c:d:hn:s:", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'c':
			if(en_stereo || en_mono){
				fprintf(stderr, "Can only record one camera at a time, please run multiple\n");
				fprintf(stderr, "instances of voxl-image-logger if you want to do that.\n");
				return -1;
			}
			en_mono = 1;
			sprintf(cam_path,"%s%s/", MODAL_PIPE_DEFAULT_BASE_DIR, optarg);
			sprintf(cam_name,"%s", optarg);
			break;

		case 'd':
			// make the directory if necessary
			if(_mkdir(optarg)) return -1;
			sprintf(save_dir,"%s", optarg);
			len = strlen(save_dir);
			// make sure directory ends in a trailing '/'
			if(save_dir[len]!='/'){
				save_dir[len]='/';
				save_dir[len+1]=0;
			}
			break;

		case 'h':
			__print_usage();
			return -1;
			break;

		case 'n':
			n_frames = atoi(optarg);
			if(n_frames<1){
				fprintf(stderr, "ERROR: n_frames must be positive\n");
				return -1;
			}
			break;

		case 's':
			if(en_stereo || en_mono){
				fprintf(stderr, "Can only record one camera at a time, please run multiple\n");
				fprintf(stderr, "instances of voxl-image-logger if you want to do that.\n");
				return -1;
			}
			en_stereo = 1;
			sprintf(cam_path,"%s%s/", MODAL_PIPE_DEFAULT_BASE_DIR, optarg);
			sprintf(cam_name,"%s", optarg);
			break;

		default:
			__print_usage();
			return -1;
		}
	}

	// check to make sure a camera was enabled
	if(!en_stereo && !en_mono){
		fprintf(stderr, "You must select either a stereo or mono camera to log\n");
		return -1;
	}

	printf("saving images to %s\n", save_dir);

	return 0;
}


// call this instead of return when it's time to exit to cleans up everything
static void _quit(int ret)
{
	pipe_client_close_all();
	if(ret==0) printf("Exiting Cleanly\n");
	exit(ret);
	return;
}


static void _index_to_path(int i, char* path, char* path2)
{
	if(en_mono){
		sprintf(path, "%s%s_%05d.png", save_dir, cam_name, i);
	}
	if(en_stereo){
		sprintf(path, "%s%s_%05d_l.png", save_dir, cam_name, i);
		if(path2!=NULL){
			sprintf(path2, "%s%s_%05d_r.png", save_dir, cam_name, i);
		}
	}
}

static void _bump_index()
{
	// increment the index counter
	current_index++;

	// check if it's time to quit
	if(n_frames>0 && (current_index-start_index)>=n_frames){
		pipe_client_set_camera_helper_cb(0, NULL);
		pipe_client_set_stereo_helper_cb(0, NULL);
		main_running = 0;
	}
	return;
}

// camera frame callback registered to voxl-camera-server
static void _mono_cb(__attribute__((unused))int ch, camera_image_metadata_t meta, char* frame)
{
	int w = meta.width;
	int h = meta.height;
	char path1[100];
	_index_to_path(current_index, path1, NULL);

	// make opencv Mat images
	if(meta.format == IMAGE_FORMAT_RAW8){
		Mat img1(h,w,CV_8UC1,frame);
		imwrite(path1, img1);
	}
	else if(meta.format == IMAGE_FORMAT_DISP16){
		Mat img1(h,w,CV_16SC1,frame);
		imwrite(path1, img1);
	}
	else{
		fprintf(stderr, "ERROR only support RAW_8 and DISP16 images right now, got %d instead\n", meta.format);
		return;
	}

	// increment the index counter
	printf("wrote %s\n", path1);
	_bump_index();

	return;
}


// camera frame callback registered to voxl-camera-server
static void _stereo_cb(__attribute__((unused))int ch, camera_image_metadata_t meta, char* left, char* right)
{
	if(meta.format != IMAGE_FORMAT_STEREO_RAW8){
		fprintf(stderr, "ERROR only support STEREO_RAW_8 images right now,got %d instead\n", meta.format);
		return;
	}

	// make opencv Mat images
	int w = meta.width;
	int h = meta.height;
	Mat img1(h,w,CV_8UC1,left);
	Mat img2(h,w,CV_8UC1,right);

	char path1[100];
	char path2[100];
	_index_to_path(current_index, path1, path2);

	// write the image to a file and increment the index counter
	imwrite(path1, img1);
	imwrite(path2, img2);
	printf("wrote %s %s\n", path1, path2);
	_bump_index();

	return;
}


int main(int argc, char* argv[])
{
	// start by parsing arguments
	if(_parse_opts(argc, argv)) return -1;

	// start signal handler so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		_quit(-1);
	}

	// search for existing log files to determine the next number in the series
	// todo be more clever and robust with this
	char path[100];
	struct stat st;
	int i;
	for(i=0; i<=10000; i++){
		_index_to_path(i,path,NULL);
		// if file exists, move onto the next index
		if(stat(path, &st) == 0) continue;
		else{
			start_index = i;
			current_index = i;
			break;
		}
	}

	_mkdir(save_dir);

	// main loop just waits and keeps trying to open pipes if they are closed
	int needs_init = 1;
	printf("opening camera pipe %s\n", cam_path);
	main_running = 1;
	while(main_running){
		if(!needs_init){
			usleep(500000);
			continue;
		}

		// still need to init, try to open a pipe
		int ret;
		if(en_mono) ret = pipe_client_init_channel(0, cam_path, CLIENT_NAME, \
											EN_PIPE_CLIENT_CAMERA_HELPER, 0);
		else ret = pipe_client_init_channel(0, cam_path, CLIENT_NAME, \
											EN_PIPE_CLIENT_STEREO_HELPER, 0);
		// success, set callback
		if(ret==0){
			pipe_client_set_pipe_size(0, MIN_PIPE_SIZE);
			if(en_mono)   pipe_client_set_camera_helper_cb(0, _mono_cb);
			if(en_stereo) pipe_client_set_stereo_helper_cb(0, _stereo_cb);
			printf("successfully opened camera pipe\n");
			needs_init=0;
		}
		// server not online yet wait.
		else if(ret == PIPE_ERROR_SERVER_NOT_AVAILABLE){
			fprintf(stderr, "waiting for camera %s\n", cam_path);
		}
		else fprintf(stderr, "ERROR: critical failure subscribing to pipe %s\n", cam_path);

		usleep(500000);
	}


////////////////////////////////////////////////////////////////////////////////
// close everything
////////////////////////////////////////////////////////////////////////////////

	_quit(0);
	return 0;
}

